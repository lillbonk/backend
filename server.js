const express = require("express");
const app = express();
const { PORT = 8000 } = process.env;
const routes = require("./app/routes/routes");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

routes(app);

app.listen(PORT, () => console.log(`Server running on ${PORT}`));
