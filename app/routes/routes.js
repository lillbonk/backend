const db = require("../config/db.config");
const { QueryTypes } = require("sequelize");
const type = { type: QueryTypes.SELECT };

const router = app => {
  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://127.0.0.1:5500");
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept"
    );
    res.header(
      "Access-Control-Allow-Methods",
      "PUT, POST, GET, DELETE, OPTIONS"
    );
    next();
  });

  app.get("/", async (req, res) => {
    try {
      return res
        .status(200)
        .json(await db.query("SELECT * FROM journal_entry", type));
    } catch (e) {
      console.log(e);
    }
  });


  //
  //Journal
  //

  app.get("/api/v1/journals", async (req, res) => {
    try {
      return res
        .status(200)
        .json(await db.query("SELECT * FROM journal", type));
    } catch (e) {
      console.log(e);
    }
  });

  app.get(`/api/v1/journals/:id`, async (req, res) => {
    try {
      return res
        .status(200)
        .json(
          await db.query(`SELECT * FROM journal where journal_id = ${id}`, type)
        );
    } catch (e) {
      console.log(e);
    }
  });

  app.post("/api/v1/journals", async (req, res) => {
    console.log(req.body.name);

    if (!req.body) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
      
    }

    try {
      await db.query(`INSERT INTO journal (name) VALUES ('${req.body.name}')`);

      res.send('BAJS!!!!!')
    } catch (e) {
      console.log(e);
    }
  });

  // Journal PATCH here
  //
  //

  app.get(`/api/v1/journals/:id/entries`, async (req, res) => {
    try {
      return res
        .status(200)
        .json(
          await db.query(
            `SELECT * FROM journal_entry where = Jorunal_id = ${id}`,
            type
          )
        );
    } catch (e) {
      console.log(e);
    }
  });

  app.delete(`/api/v1/journals/:id`, async (req, res) => {
    try {
      return res
        .status(200)
        .json(await db.query(`delete from journal where journal_id = ${id};`));
    } catch (e) {
      console.log(e);
    }
  });

  // Journal Entries

  app.get("/api/v1/entries", async (req, res) => {
    try {
      return res
        .status(200)
        .json(await db.query("SELECT * FROM journal_entry", type));
    } catch (e) {
      console.log(e);
    }
  });

  app.get(`/api/v1/entries/:id`, async (req, res) => {
    try {
      return res
        .status(200)
        .json(
          await db.query(
            `SELECT * FROM journal_entry where journal_entry_id = ${id}`,
            type
          )
        );
    } catch (e) {
      console.log(e);
    }
  });

  app.get(`/api/v1/entries/:id/tags`, async (req, res) => {
    try {
      return res.status(200).json(
        await db.query(
          `SELECT je.title, je.text, je.created_at, CONCAT(a.first_name, ' ', a.last_name) AS author, GROUP_CONCAT(t.name) AS tags
          FROM journal_entry AS je
          LEFT JOIN journal_tag AS jt using (journal_entry_id)
          LEFT JOIN tag AS t using (tag_id)
          INNER JOIN author a ON je.author_id = a.id
          WHERE je.journal_entry_id = ${id}
          GROUP BY je.journal_entry_id`,
          type
        )
      );
    } catch (e) {
      console.log(e);
    }
  });

  app.post("/api/v1/entries", async (req, res) => {
    try {
      await db.query("INSERT INTO journal_entry SET ?", req.body);
      res.status(201).send(`Journal_entry added with ID: ${res.insertId}`);
    } catch (e) {
      console.log(e);
    }
  });

  //
  // ENTRIES PATCH HERE
  //

  app.delete(`/api/v1/entries/:id`, async (req, res) => {
    try {
      return res
        .status(200)
        .json(
          await db.query(
            `delete from journal_entry where journal_entry_id = ${id};`
          )
        );
    } catch (e) {
      console.log(e);
    }
  });

  // AUTHORS

  app.get("/api/v1/authors", async (req, res) => {
    try {
      return res.status(200).json(await db.query("SELECT * FROM author", type));
    } catch (e) {
      console.log(e);
    }
  });

  app.get(`/api/v1/authors/:id`, async (req, res) => {
    try {
      return res
        .status(200)
        .json(await db.query(`SELECT * FROM author where id = ${id}`, type));
    } catch (e) {
      console.log(e);
    }
  });

  app.post("/api/v1/authors", async (req, res) => {
    try {
      await db.query("INSERT INTO authors SET ?", req.body);
      res.status(201).send(`Author added with ID: ${res.insertId}`);
    } catch (e) {
      console.log(e);
    }
  });

  //
  // AUTHOR PATCH HERE

  app.delete(`/api/v1/authors/:id`, async (req, res) => {
    try {
      return res
        .status(200)
        .json(await db.query(`delete from author where id = ${id};`));
    } catch (e) {
      console.log(e);
    }
  });

  //TAGS

  app.get("/api/v1/tags", async (req, res) => {
    try {
      return res.status(200).json(await db.query("SELECT * FROM tag", type));
    } catch (e) {
      console.log(e);
    }
  });

  app.get(`/api/v1/tags/:id`, async (req, res) => {
    try {
      return res
        .status(200)
        .json(await db.query(`SELECT * FROM tag where id = ${id}`, type));
    } catch (e) {
      console.log(e);
    }
  });
};

module.exports = router;
